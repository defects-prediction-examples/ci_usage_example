FROM python:3.5.2-slim

ADD requirements.txt requirements.txt

RUN pip install --upgrade pip && \
    pip install -r requirements.txt