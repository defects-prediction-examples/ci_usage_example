import pandas
import scipy
import sklearn
import pytest


class TestLibraries:
    def test_pytest_version(self):
        assert pytest.__version__ == '3.1.3'

    def test_pandas_version(self):
        assert pandas.__version__ == '0.20.3'

    def test_scipy_version(self):
        assert scipy.__version__ == '0.19.1'

    def test_scikit_learn_version(self):
        assert sklearn.__version__ == '0.18.2'

    def test_hello():
        assert False