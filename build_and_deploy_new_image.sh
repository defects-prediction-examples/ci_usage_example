#!/usr/bin/env bash

REGISTRY="registry.gitlab.com"
GROUP_NAME="defects-prediction-examples"
IMAGE_NAME="${GROUP_NAME}/ci_usage_example"
FULL_IMAGE_NAME=${REGISTRY}/${IMAGE_NAME}

docker login ${REGISTRY}
docker build -t ${FULL_IMAGE_NAME} .
docker push ${FULL_IMAGE_NAME}
